
Welcome to the Bicycle Pump project.

Bicycle Pump is a collection of ansible playbooks that can be used to create a network build server, using the age old technology known as PXE. 

The playbooks can install everything you need incluing a DHCP server if rquired.

Main setup file (setup.yml)

This is the main playbeook that will 

* install a tftp server and configure it for PXE booting
* install and configure a DHCP server (can be disabled if not required)
* install a webserver 

Operating System Setup Files 

Once the base system is setup there are a number of other playbooks that will install and configure the Bicycle Pump server for installing certain operating systems. These playbooks are currently

* Fedora 29 Server
* Centos 7
* Red Hat Enterprise Linux 7
* Red Hat Enterprise Linux 8 Beta

There is also some support for CoreOS

* CoreOS Boot from Network (no install)
* CoreOS Install from Network (currently not completed)

Using the Operating System Setup Files

In order to use the Operating System Setup Files, you will also have to edit the vars file (vars/main.yml), to enable each Operating System (by setting the boolean to true and supplying a path to an install ISO). Once the vars file is editedm, you can goahead and run the playbook. 

For example, to enable Bicycle Pump to deploy Fedora 29 you would take these steps

* downoad the Fedora 29 Server ISO
* edit the vars file (as seen below)
* run hte playbook

The vars/main.yml should be edited to look like this 

```
...
f29: true
f29_iso: ~/ISOs/Fedora-Server-dvd-x86_64-29-1.2.iso
...
```

Running
